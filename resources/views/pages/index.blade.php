@extends('layouts.app')
@section('content')

    <div class="jumbotron text-center">
    	<h2>Home Page</h2>
        <p>This is Laravel CRUD Application</p>
        <p>
            <a class="btn btn-primary" href="/login">Login</a>
            <a class="btn btn-primary" href="/register">Register</a>
        </p>
    </div>
@endsection