@extends('layouts.app')

@section('content')
<div class="form-group">
	<a href="/users" class="btn btn-default">Go Back</a>	
</div>

<div class="well">
<h3><strong>User Name: </strong> {{$user->username}}</h3>
<h3><strong>Email: </strong> {{$user->email}}</h3>
<small><strong>Written on: </strong> {{$user->created_at}}</small>	
</div>
<a href="/users/{{$user->id}}/edit" class="btn btn-primary">Edit</a>

{!!Form::open(['action' =>['UsersController@destroy', $user->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
{{Form::hidden('_method', 'DELETE')}}
{{Form::submit('DELETE', ['class' => 'btn btn-danger'])}}
{!! Form::close() !!}
@endsection