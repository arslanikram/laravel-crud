@extends('layouts.app')

@section('content')
<h1>Edit User</h1>
{!! Form::open(['action'  => ['UsersController@update', $user->id], 'method' => 'POST']) !!}
<div class="form-group">
	{{Form::label('username','User Name')}}
	{{Form::text('username',$user->username,['class' => 'form-control', 'placeholder' => 'User Name'])}}
</div>
<div class="form-group">
	{{Form::label('email','Email')}}
	{{Form::email('email',$user->email,['class' => 'form-control', 'placeholder' => 'Email'])}}
</div>
<div class="form-group">
	{{Form::label('password','Password')}}
	{{Form::password('password',['class' => 'form-control', 'placeholder' => 'Password'])}}
</div>
{{Form:: hidden('_method','PUT')}}
{{Form:: submit('Submit',['class' => 'btn btn-primary'])}}
    
{!! Form::close() !!}
@endsection