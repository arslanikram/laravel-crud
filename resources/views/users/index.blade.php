@extends('layouts.app')

@section('content')
<h1>Users Data</h1>
@if(count($users)>0)
	@foreach($users as $user)
		<div class="well">
			<h4><a href="/users/{{$user->id}}">{{$user->username}}</a></h4>
			<h4>{{$user->email}}</h4>
		</div>
	@endforeach
	{{$users->links()}}
@else
	<p>No Record Found in Database</p>
@endif

@endsection